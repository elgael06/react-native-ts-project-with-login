# BASE APP WITH LOGIN

## USE TECH:

#### packeges:

- Navigation.

  - @react-navigation/native.
  - @react-navigation/native-stack.
  - [Documentation](https://reactnavigation.org/docs/getting-started/).

- Kit of interfaces(UI).
  - @ui-kitten/components
  - [Documentation](https://akveo.github.io/react-native-ui-kitten/docs/components/)

- Global App state.
  - jotai.
  - [Documentation](https://jotai.org/).

- Local moduleState.
  - use hooks context.
  - [Documentation](https://reactjs.org/docs/hooks-reference.html#usecontext).