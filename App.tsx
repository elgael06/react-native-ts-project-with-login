import React from 'react';
import {useColorScheme} from 'react-native';

import * as eva from '@eva-design/eva';
import {ApplicationProvider} from '@ui-kitten/components';

import MyStack from './src/routes/Route.routes';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = isDarkMode ? eva.dark : eva.light;

  return (
    <ApplicationProvider {...eva} theme={backgroundStyle}>
      <MyStack />
    </ApplicationProvider>
  );
};

export default App;
