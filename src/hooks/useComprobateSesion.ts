import {useState} from 'react';
import {IComprobateSesion, ISesionState} from '../interfaces/IState';

const useComprobateSesion = (
  email: string,
  password: string,
): IComprobateSesion => {
  const [sesion, setSesion] = useState<ISesionState>({
    isLogged: false,
  });
  const [loading, setLoading] = useState(false);

  const submit = async () => {
    setLoading(true);
    console.log(email, password);
    await new Promise((resolve: any) => setTimeout(resolve, 2000));
    setSesion({
      ...sesion,
      isLogged: true,
    });
    setLoading(false);
  };

  return {
    ...sesion,
    loading,
    submit,
  };
};

export default useComprobateSesion;
