import {atom} from 'jotai';
import {ISesionState} from '../interfaces/IState';

export const sesionInitialState: ISesionState = {
  isLogged: false,
};

export const sesionState = atom<ISesionState>(sesionInitialState);
