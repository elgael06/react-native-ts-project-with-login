import React from 'react';
import {Button, Text, View} from 'react-native';

export default () => {
  const back = () => {
    console.log('regresar');
  };

  return (
    <View
      style={{
        height: 50,
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'space-between',
        background: 'red',
    }}>
      <Button onPress={back} title='title' />
      <Text>Application name</Text>
      <Button onPress={back} title='title' />
    </View>
  );
};
