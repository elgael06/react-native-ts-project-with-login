import React from 'react';
import {Layout} from '@ui-kitten/components';
import {StyleSheet} from 'react-native';

export default ({children}) => (
  <Layout level="1" style={[styles.root]}>{children}</Layout>);


const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    padding: 20,
    marginTop: -40,
  },
});
