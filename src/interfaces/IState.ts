//
export interface ISesionState {
  isLogged: boolean;
}

export interface IComprobateSesion extends ISesionState {
  loading: boolean;
  submit: () => Promise<void>;
}
