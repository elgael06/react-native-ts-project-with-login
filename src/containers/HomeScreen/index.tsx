import React, {useEffect} from 'react';
import {useAtom} from 'jotai';
// import {Button} from 'react-native';
import {Button} from '@ui-kitten/components';
import Layout from '../../components/Layout';
import {sesionInitialState, sesionState} from '../../store/globalState';

export default ({navigation}) => {
  const [sesion, setSesion] = useAtom(sesionState);

  useEffect(() => {
    if (!sesion.isLogged) {
      navigation?.popToTop();
    }
  }, [navigation, sesion]);

  // handdle event
  const handdleLogout = () => {
    setSesion({...sesionInitialState});
  };

  return (
    <Layout>
      <Button onPress={() => navigation?.navigate('Items')}>Ir A Lista</Button>
      <Button status="danger" onPress={handdleLogout}>
        SALIR
      </Button>
    </Layout>
  );
};
