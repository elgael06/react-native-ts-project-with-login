import React, {useEffect, useMemo, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, Input,Spinner,Text} from '@ui-kitten/components';
import Layout from '../../components/Layout';
import {useAtom} from 'jotai';
import {sesionState} from '../../store/globalState';
import useComprobateSesion from '../../hooks/useComprobateSesion';

export default ({navigation}) => {
  const [sesion, setSesion] = useAtom(sesionState);
  const [values, setValues] = useState<{email: string; password: string}>({
    email: '',
    password: '',
  });
  const {submit, loading} = useComprobateSesion(values.email, values.password);

  const verificDisable = useMemo(
    () => !values.email.length || !values.password.length,
    [values],
  );

  useEffect(() => {
    if (sesion.isLogged) {
      navigation?.navigate('Home');
    }
    return () => {
      setValues({
        email: '',
        password: '',
      });
    };
  }, [navigation, sesion]);

  // handdle event
  const handleSubmit = () => {
    console.log(values);
    !verificDisable &&
      submit().then(() => {
        setSesion({
          ...sesion,
          isLogged: true,
        });
      });
  };

  const LoadingIndicator = () => (
    <View style={styles.leadding}>{loading && <Spinner size="small" />}</View>
  );

  return (
    <Layout>
      <Text status="primary" category="h1">
        Login
      </Text>
      <Input
        style={[styles.textInput]}
        label="Usuario"
        placeholder="Introduce tu nombre de usuario..."
        disabled={loading}
        value={values.email}
        onChangeText={email => setValues({...values, email})}
      />
      <Input
        style={[styles.textInput]}
        label="Password"
        placeholder="Introduce tu Password..."
        secureTextEntry={true}
        disabled={loading}
        value={values.password}
        onChangeText={password => setValues({...values, password})}
      />
      <Button
        style={styles.btn}
        disabled={verificDisable || loading}
        appearance="filled"
        status="primary"
        accessoryLeft={LoadingIndicator}
        onPress={handleSubmit}>
        {loading ? 'Cargando...' : 'ENTRAR'}
      </Button>
    </Layout>
  );
};

const styles = StyleSheet.create({
  title: {fontSize: 30},
  textInput: {
    marginBottom: 10,
    marginTop: 10,
  },
  leadding: {justifyContent: 'center', alignItems: 'center'},
  btn: {
    marginTop: 10,
  },
});
